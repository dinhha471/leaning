package service

import (
	"fmt"
    "../model"
    "bufio"
    //"flag"
    "log"
    "os"
    "strings"
    "strconv"
)

//var listEmployee map[int]interface{} = map[int]interface{}{}
var listEmployee map[int]model.Employee

// load and cache data to the map
func loadUserData() {
    if listEmployee == nil {
        readAllDataFromFile()
    }
}

// get a employee by id
func GetUser(id int) *model.Employee {
    loadUserData()
    for _, v := range listEmployee {
        if v.Id == id {
            return &v
        }
    }
    return nil
}

// get all employee from file
func GetAllUser() map[int]model.Employee {
    loadUserData()
    return listEmployee
}

// update employee information
func UpdateEmployeeInformation(id int, employee model.Employee) {
    loadUserData()
    listEmployee[id] = employee
    saveToFile()
}

// create new employee
func CreateNewEmployee(employee model.Employee) {
    loadUserData()
    newId := employee.Id
    listEmployee[newId] = employee
    saveToFile()
}

// delete an employee
func DeleteEmployee(id int) {
    loadUserData()
    delete(listEmployee, id)
    saveToFile()
}

// search employee by salary
func SearchBySalary(salary float64) []model.Employee {
    loadUserData();
    result := make([]model.Employee, len(listEmployee))
    index := 0
    for _, v := range listEmployee {
        if v.Salary >= salary {
            result[index] = v
            index ++
        }
    }
    return result[:index]
}

func SearchByName(name string) (*model.Employee, bool) {
    loadUserData();
    
    for _, v := range listEmployee {
        if strings.Compare(v.Name, name) == 0 {
            return &v, true
        }
    }

    return nil, false
}


func readAllDataFromFile() {
    f, err := os.Open("../resource/data.txt");
    if err != nil {
        log.Fatal(err)
    }

    defer func() {
        if err = f.Close(); err != nil {
            log.Fatal(err)
        }
    }()
    //var listEmployee map[int]interface{} = map[int]interface{}{}
    listEmployee = map[int]model.Employee{}
    s := bufio.NewScanner(f)
        for s.Scan() {
            line := s.Text()
            items := strings.Split(line, ",")
            e := convertToEmployee(items)
            listEmployee[e.Id] = e
            fmt.Println(items)
        }

        err = s.Err()
    if err != nil {
        log.Fatal(err)
    }
}

func convertToEmployee(items []string) model.Employee {
    var id int
    var name string
    var salary float64
    var age int
    if i, err := strconv.Atoi(items[0]); err == nil {
        id = i
    }
    name = items[1]
    if s, err := strconv.ParseFloat(items[2], 64); err == nil {
        salary = s
    }
    if a, err := strconv.Atoi(items[3]); err == nil {
        age = a
    }

    e := model.Employee{Id: id, Name: name, Salary:salary, Age: age}
    return e
}

func saveToFile() {
    f, err := os.Create("../resource/data.txt")
    if err != nil {
        fmt.Println(err)
        return
    }
    for _, v := range listEmployee {
        record := convertEmployeeToString(v)
        _, err := f.WriteString(record)
        if err != nil {
            fmt.Println(err)
            f.Close()
            return
        }
    }

    err = f.Close()
    if err != nil {
        fmt.Println(err)
        return
    }
}

func convertEmployeeToString(employee model.Employee ) string {
    id := strconv.Itoa(employee.Id)
    name := employee.Name
    salary := fmt.Sprintf("%.0f", employee.Salary)
    age := strconv.Itoa(employee.Age)
    result := id + "," + name + "," + salary + "," + age + "\n"
    return result
}