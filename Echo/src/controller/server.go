package main

import (
    "net/http"
    "strconv"

    "../service"
    "../model"

    "github.com/labstack/echo"
)

var json map[string]interface{} = map[string]interface{}{}

func main() {
    e := echo.New()
    e.GET("/users/:id", getUser)
    e.GET("/users", getAllUser)
    e.GET("users/search/salary/:salary", searchBySalary)
    e.GET("users/search/name/:name", searchByName)
    e.POST("/users/:id", updateEmployee)
    e.PUT("/users", createNewEmployee)
    e.DELETE("/users/:id", deleteAnEmployee)
    e.Logger.Fatal(e.Start(":1323"))
}

// get employee by id
func getUser(c echo.Context) error {
    // User ID from path `users/:id`
    id, err := strconv.Atoi(c.Param("id"))
    if err != nil {
        return err
    } else {
        response := service.GetUser(id)
        return c.JSON(http.StatusOK, response)
    }
}

// get employee user
func getAllUser(c echo.Context) error {
    response := service.GetAllUser();
    return c.JSON(http.StatusOK, response)
}

// update employee information
func updateEmployee(c echo.Context) error {
    var input model.Employee
    if err := c.Bind(&input); err != nil {
        return err
    }

    // User ID from path `users/:id`
    id, err := strconv.Atoi(c.Param("id"))
    if err != nil {
        return err
    } else {
        service.UpdateEmployeeInformation(id, input)
        return c.String(http.StatusOK, "Updated successful")
    }
}

// create new employee
func createNewEmployee(c echo.Context) error {
    var input model.Employee
    if err:= c.Bind(&input); err != nil {
        return err
    }

    service.CreateNewEmployee(input)
    return c.String(http.StatusOK, "Created successful")
}

//delete an employee
func deleteAnEmployee(c echo.Context) error {
    id, err := strconv.Atoi(c.Param("id"))
    if err != nil {
        return err
    } else {
        service.DeleteEmployee(id)
        return c.String(http.StatusOK, "Deleted successful")
    }
}

// search by salary
func searchBySalary(c echo.Context) error {
    salary, err := strconv.ParseFloat(c.Param("salary"), 64)
    if err != nil {
        return err
    } else {
        response := service.SearchBySalary(salary)
        return c.JSON(http.StatusOK, response)
    }
}

// search by name
func searchByName(c echo.Context) error {
    name := c.Param("name")
    response, found := service.SearchByName(name)
    if found == true {
        return c.JSON(http.StatusOK, response)
    } else {
        return c.String(http.StatusOK, "Couldn't found this employee!")
    }
}

