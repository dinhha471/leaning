package model

type Employee struct {
    Id     int
    Name   string
    Salary float64
    Age    int
}

func NewEmployee(id int, name string, salary float64, age int) *Employee {
    e := Employee{Id: id, Name: name, Salary: salary, Age: age}
    return &e
}
